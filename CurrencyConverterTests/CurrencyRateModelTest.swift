//
//  CurrencyRateModelTest.swift
//  CurrencyConverterTests
//
//  Created by Chinchkhede, Ashwini on 2020/05/19.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import XCTest
@testable import CurrencyConverter


class CurrencyRateModelTest: XCTestCase {
    
    var currencyRateModel : CurrencyRateModel?
    var currencyRate : CurrencyRate?
    
    override func setUp() {
        currencyRateModel = CurrencyRateModel()
    }
    
    override func tearDown() {
        currencyRateModel = nil
        currencyRate = nil
    }
    
    func testIfNewInstanceCreated() {
        XCTAssertNotNil(currencyRateModel)
    }
    
    func testIfBaseURLStringCorrect() {
        let baseURLString = AppConstants.baseUrl+"\(AppConstants.live)?access_key=\(AppConstants.accessKey)&format=1"
        let expectedBaseURLString = "http://api.currencylayer.com/live?access_key=d2d9c48931565c7c55dbdc6251312c14&format=1"
        XCTAssertEqual(baseURLString, expectedBaseURLString, "Base URL does not match expected URL")
    }
    
    
    
    func testFetchCurrencyListAPI(){
        let expectations = expectation(description: "Server exception")
        let url = URL(string: AppConstants.baseUrl+"\(AppConstants.live)?access_key=\(AppConstants.accessKey)&format=1")!
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if(error == nil){
                do {
                    self.currencyRate = try JSONDecoder().decode(CurrencyRate.self, from: data!)
                    XCTAssertNotNil(self.currencyRate)
                    if let value = self.currencyRate!.quotes.first(where: { $0.key == "USDBIF" })?.value {
                        XCTAssertEqual(value, 1911.0,"Key:USDBIF = Value: 1911.0")
                    }
                    expectations.fulfill()
                } catch {
                    print("Rate Parsing error")
                }
            }else{
                XCTFail("Server response failed : \(error!.localizedDescription)")
            }
            
        }.resume()
        
        //wait for some time for the expectation (you can wait here more than 30 sec, depending on the time for the response)
        self.waitForExpectations(timeout: 30, handler: { (error) in
            if let error = error {
                print("Failed : \(error.localizedDescription)")
            }
            
        })
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
