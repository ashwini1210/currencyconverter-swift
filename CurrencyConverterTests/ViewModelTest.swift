//
//  ViewModelTest.swift
//  CurrencyConverterTests
//
//  Created by Chinchkhede, Ashwini on 2020/05/19.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import XCTest
@testable import CurrencyConverter

class ViewModelTest: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testIfCalculationOfJPYIntoAEDIsCorrect() {
        let jpyData = CurrencyRate(success: true, timestamp: 1589819705, source: "USD", quotes: ["USDJPY":107.27195])
        let aedData = CurrencyRate(success: true, timestamp: 1589819705, source: "USD", quotes: ["USDAED":3.672976])
        
        let baseRate = 2.0
        for (_, sourceCurrencyRate) in jpyData.quotes {
            let rate = 1 / sourceCurrencyRate
            for (_, destCurrencyRate) in aedData.quotes {
                let calculatedCurrency =  baseRate * rate * destCurrencyRate
                let approxCalculatedCurrency = String(format:"%.02f",calculatedCurrency)
                XCTAssertEqual(approxCalculatedCurrency, "0.07", "Conversion of 2JPY into AED = 0.068, Approximate is 0.07")
            }
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
