//
//  ViewControllerTest.swift
//  CurrencyConverterUITests
//
//  Created by Chinchkhede, Ashwini on 2020/05/19.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import XCTest
@testable import CurrencyConverter

class ViewControllerTest: XCTestCase {
    
    var controller: ViewController!
    var viewModel: ViewModel!
    
    override func setUp() {
        continueAfterFailure = false
        
        controller = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? ViewController
        controller.loadViewIfNeeded()
        viewModel = ViewModel(controller!)
    }
    
    override func tearDown() {
        controller = nil
        viewModel = nil
    }
    
    func testIfTextFieldAreConnected() throws {
        _ = try XCTUnwrap(controller!.amountTextview, "Amount UITextField is not connected")
    }
    
    func testIfCollectionViewExist(){
        XCTAssertNotNil(controller.ratesCollectionView,"Controller should have a CollectionView")
    }
    
    func testIfTableViewExist(){
        XCTAssertNotNil(controller.currencyTableview,"Controller should have a TableView")
    }
    
    func testIfButtonHasActionAssigned() {
        
        //Check if Controller has UIButton property
        let currencyButton: UIButton = controller!.currencyButton
        XCTAssertNotNil(currencyButton, "View Controller does not have UIButton property")
        
        // Attempt Access UIButton Actions
        guard let currencyButtonActions = currencyButton.actions(forTarget: controller, forControlEvent: .touchUpInside) else {
            XCTFail("UIButton does not have actions assigned for Control Event .touchUpInside")
            return
        }
        
        // Assert UIButton has action with a method name
        XCTAssertTrue(currencyButtonActions.contains("currencyButtonClicked:"))
        
    }
    
    
}
