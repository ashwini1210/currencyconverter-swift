//
//  CurrencyListModelTest.swift
//  CurrencyConverterTests
//
//  Created by Chinchkhede, Ashwini on 2020/05/19.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import XCTest
@testable import CurrencyConverter

class CurrencyListModelTest: XCTestCase {
    
    var currencyListModel : CurrencyListModel?
    var currencyList : CurrencyList?
    
    override func setUp() {
        currencyListModel = CurrencyListModel()        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testIfNewInstanceCreated() {
        XCTAssertNotNil(currencyListModel)
    }
    
    func testIfBaseURLStringCorrect() {
        let baseURLString = AppConstants.baseUrl+"\(AppConstants.list)?access_key=\(AppConstants.accessKey)&format=1"
        let expectedBaseURLString = "http://api.currencylayer.com/list?access_key=d2d9c48931565c7c55dbdc6251312c14&format=1"
        XCTAssertEqual(baseURLString, expectedBaseURLString, "Base URL does not match expected URL")
    }
    
    
    
    func testFetchCurrencyListAPI(){
        let expectations = expectation(description: "Server exception")
        let url = URL(string: AppConstants.baseUrl+"\(AppConstants.list)?access_key=\(AppConstants.accessKey)&format=1")!
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if(error == nil){
                do {
                    self.currencyList = try JSONDecoder().decode(CurrencyList.self, from: data!)
                    XCTAssertNotNil(self.currencyList)
                    if let value = self.currencyList!.currencies.first(where: { $0.key == "AED" })?.value {
                        XCTAssertEqual(value, "United Arab Emirates Dirham","AED = United Arab Emirates Dirham")
                    }
                    expectations.fulfill()
                } catch {
                    print("List Parsing error")
                }
            }else{
                XCTFail("Server response failed : \(error!.localizedDescription)")
            }
            
        }.resume()
        
        //wait for some time for the expectation (you can wait here more than 30 sec, depending on the time for the response)
        self.waitForExpectations(timeout: 30, handler: { (error) in
            if let error = error {
                print("Failed : \(error.localizedDescription)")
            }
            
        })
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
