//
//  ViewModel.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/18.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class ViewModel: NSObject {
    var viewController: ViewController?
    var listModel : CurrencyListModel?
    var rateModel : CurrencyRateModel?
    var currencyListRealmManager : CurrencyListRealmManager?
    var currencyRateRealmManager : CurrencyRateRealmManager?
    var quotesArray = [(key: String, value: Double)]()
    var timer = Timer()
    var baseRate = AppConstants.baseRate
    
    init(_ viewController: ViewController) {
        super.init()
        self.viewController = viewController
        initInstance()
        updateServerValue()
    }
    
    // MARK: Private Methods
    /*
     * Function to initialize variables
     */
    private func initInstance(){
        self.currencyListRealmManager = CurrencyListRealmManager()
        self.currencyRateRealmManager = CurrencyRateRealmManager()
    }
    
    /*
     * Function to update Currency Rate after every 30min
     */
    private func updateServerValue(){
        self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(AppConstants.thirtyMinute), target: self, selector: #selector(self.initializeCurrencyRateModel), userInfo: nil, repeats: true)
    }
    
    
    // MARK: Public Methods
    func showKeyboard(){
        if let vc = self.viewController{
            vc.amountTextview.becomeFirstResponder()
        }
    }
    
    func hideKeyboard(){
        if let vc = self.viewController{
            vc.amountTextview.resignFirstResponder()
        }
    }
    
    /*
     * Function to call Currency List API
     */
    func initializeCurrencyListModel(){
        listModel = CurrencyListModel()
        listModel!.delegate = self
        listModel!.fetchData()
        
    }
    
    /*
     * Function to call Currency Rate API
     */
    @objc func initializeCurrencyRateModel(){
        rateModel = CurrencyRateModel()
        rateModel!.delegate = self 
        rateModel!.fetchData()
    }
    
    /*
     * Function to return currency Rate
     */
    func getAllCurrencyRate() -> CurrencyRate?{
        if let rateModel = self.currencyRateRealmManager{
            let currencyRate = rateModel.getCurrencyRate()
            return currencyRate
        }
        return nil
    }
    
    /*
     * Function to return currency list
     */
    func getAllCurrencyList() -> CurrencyList?{
        if let listModel = self.currencyListRealmManager{
            let currencyList = listModel.getCurrencyList()
            return currencyList
        }
        return nil
    }
    
    /*
     * Function to call list and live API
     */
    func initializeModeForServerResponse(){
        if let vc = self.viewController{
            if(Utility.shared.checkInternetConnection()){
                initializeCurrencyListModel()
                initializeCurrencyRateModel()
            }else{
                Utility.shared.displayAlert(viewController: vc)
            }
        }
    }
    
    /*
     * Function to sort rate data as per key
     */
    func sortDataByCurrencyName(){
        let currencyRate = self.getAllCurrencyRate()
        if let rate = currencyRate {
            quotesArray = rate.quotes.map { $0 }.sorted { $0.key < $1.key }
        }
    }
    
    /*
     * Function to calculate currency amount on basis of live currency rate
     */
    func getBaseAmount(){
        if let vc = self.viewController{
            if let amount = vc.amountTextview.text{
                if let amt = Double(amount){
                    baseRate = amt
                }else{
                    baseRate = 0
                }
            }
        }
    }
    
    func getLiveSelectedCurrencyValue() -> Double{
        if let vc = self.viewController{
            let selectedCurrency = vc.currencyButton.title(for: .normal)
            let keyValPair = quotesArray.filter { $0.key == "USD\(selectedCurrency!)" }
            if(keyValPair.count != 0){
                let value = (keyValPair[0].value)
                return value
            }
        }
        return 0.0
    }
    
    func displayUpdatedRate(){
        if let vc = self.viewController{
            getBaseAmount()
            vc.ratesCollectionView.reloadData()
        }
    }
    
    /*
     * Currency calculation logic
     */
    func calculateCurrencyRate(indexPath: IndexPath) -> Double?{
        let currencyRate = self.getLiveSelectedCurrencyValue()
        let rate = 1 / currencyRate
        let value = self.baseRate * rate * self.quotesArray[indexPath.row].value
        return value
    }
}


extension ViewModel : HttpResponseProtocol{
    /*
     * Function to handle server response in viewmodel
     */
    func onHttpResponse(isSuccess: Bool, response: Any?, error: Any?, anyIdentifier: String?) {
        if(isSuccess){
            if(anyIdentifier == AppConstants.list){
                if let vc = self.viewController{
                    vc.currencyTableview.reloadData()
                }
            }
            else{
                if let vc = self.viewController{
                    sortDataByCurrencyName()
                    vc.ratesCollectionView.reloadData()
                }
            }
        }
    }
}
