//
//  CurrencyRateModel.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class CurrencyRateModel: BaseModel {
    var currencyRate : CurrencyRate?
    var currencyRateRealmManager : CurrencyRateRealmManager?

    /*
     * Function to hit API and get server response for live currency rate
     */
    public func fetchData(){
        let url = URL(string: AppConstants.baseUrl+"\(AppConstants.live)?access_key=\(AppConstants.accessKey)&format=1")!
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if(error == nil){
                do {
                    self.currencyRate = try JSONDecoder().decode(CurrencyRate.self, from: data!)
                } catch {
                    print("Rate Parsing error")
                }
                DispatchQueue.main.async {
                    if let data = self.currencyRate{
                        self.currencyRateRealmManager = CurrencyRateRealmManager()
                        self.currencyRateRealmManager!.deleteData()
                        self.currencyRateRealmManager!.saveCurrencyRate(currencyRate: data)
                    }
                    self.delegate!.onHttpResponse(isSuccess: true, response: self.currencyRate, error: nil, anyIdentifier: AppConstants.live)
                }
            }else{
                print("Rate Fetching Error: \(String(error!.localizedDescription))")
            }
        }.resume()
    }
}
