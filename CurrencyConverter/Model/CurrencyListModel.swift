//
//  CurrencyListModel.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class CurrencyListModel: BaseModel {
    var currencyList : CurrencyList?
    
    /*
     * Function to hit API and get server response for currency List
     */
    public func fetchData(){
        let url = URL(string: AppConstants.baseUrl+"\(AppConstants.list)?access_key=\(AppConstants.accessKey)&format=1")!
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if(error == nil){
                do {
                    self.currencyList = try JSONDecoder().decode(CurrencyList.self, from: data!)
                } catch {
                    print("List Parsing error")
                }
                DispatchQueue.main.async {
                    if let data = self.currencyList{
                        CurrencyListRealmManager().saveCurrencyList(currencyList: data)
                    }
                    self.delegate!.onHttpResponse(isSuccess: true, response: self.currencyList, error: nil, anyIdentifier: AppConstants.list)
                }
            }else{
                print("List Fetching Error: \(String(error!.localizedDescription))")
            }
        }.resume()
    }
}
