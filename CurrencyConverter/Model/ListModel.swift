//
//  ListModel.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

struct CurrencyList : Decodable {
    let success: Bool
    let currencies : [String:String]
}

//http://api.currencylayer.com/list?access_key=d2d9c48931565c7c55dbdc6251312c14&format=1
class CurrencyListModel: NSObject {
    var currencyList : CurrencyList?
    var delegate : HttpResponseProtocol?
    
    func fetchData(){
        let url = URL(string: AppConstants.BaseUrl+"\(AppConstants.list)?access_key=\(AppConstants.AccessKey)&format=1")!
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if(error == nil){
                do {
                    self.currencyList = try JSONDecoder().decode(CurrencyList.self, from: data!)
                } catch {
                    print("List Parsing error")
                }
                DispatchQueue.main.async {
                    self.delegate!.onHttpResponse(isSuccess: true, response: self.currencyList, error: nil, anyIdentifier: nil)
                }
            }else{
                print("List Fetching Error: \(error.debugDescription)")
            }
        }.resume()
    }
}
