//
//  CurrencyListStruct.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import Foundation

struct CurrencyList : Decodable {
    let success: Bool
    let currencies : [String:String]
}
