//
//  CurrencyRate.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import Foundation

struct CurrencyRate : Decodable {
    let success: Bool
    let timestamp: Int
    let source: String
    let quotes : [String:Double]
}
