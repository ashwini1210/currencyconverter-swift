//
//  Utility.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    static let shared = Utility()
    
    /*
     * Function to set border on the table view
     */
    func setBorderToTableView(view:UIView,cornerRadius:CGFloat){
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = cornerRadius
        view.clipsToBounds = true
    }
    
    /*
     * Function to check internet connection
     */
    func checkInternetConnection() -> Bool{
        var isInternetAvailable = true
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            isInternetAvailable = false
        case .online(.wwan):
            isInternetAvailable = true
        case .online(.wiFi):
            isInternetAvailable = true
        }
        return isInternetAvailable
    }
    
    /*
    * Function to display ALert if No internet connection
    */
    func displayAlert(viewController:ViewController){
        let alert = UIAlertController(title: AppConstants.internetTitle, message: AppConstants.internetError, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: AppConstants.button, style: .default, handler: { action in
            switch action.style{
            case .default:
                viewController.viewModel!.initializeModeForServerResponse()
                break
            case .cancel:
                break
            case .destructive:
                break
            @unknown default:
                fatalError()
            }}))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    
}
