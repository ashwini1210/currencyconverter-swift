//
//  AppConstants.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class AppConstants: NSObject {
    
    //MARK: Number constant
    static let animationDuration = 0.3
    static let thirtyMinute = 1800
    static let baseRate = 1.0

    
    //MARK: Tableview constant
    static let currencyTableviewIdentifier = "currencyCell"
    static let unitValue = "1"

    
    //MARK: CollectionView constant
    static let cellName = "RatesCollectionViewCell"
    
    //MARK: Model constant
    static let accessKey = "d2d9c48931565c7c55dbdc6251312c14"
    static let baseUrl = "http://api.currencylayer.com/"
    static let list = "list"
    static let live = "live"
    
    //MARK: Internet error constant
    static let internetTitle = "No Internet"
    static let internetError = "Internet connection not available \n Please try again!"
    static let button = "Retry"
}
