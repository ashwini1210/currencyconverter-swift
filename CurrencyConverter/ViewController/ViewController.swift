//
//  ViewController.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var amountTextview: UITextField!
    @IBOutlet weak var currencyButton: UIButton!
    @IBOutlet weak var currencyTableview: UITableView!
    @IBOutlet weak var currencyTableviewHeight: NSLayoutConstraint!
    @IBOutlet weak var ratesCollectionView: UICollectionView!
    
    //MARK: - Variables
    var isCurrencyTableviewVisible = false
    var viewModel : ViewModel?
    
    // MARK: View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initValues()
        initInstance()
        setBorderToButton()
        setBorderToTableView()
        registerCollectionView()
        setBorderToCollectionView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let viewModel = viewModel{
            viewModel.initializeModeForServerResponse()
        }
    }
    
    // MARK: Private Methods
    private func initValues(){
        currencyTableviewHeight.constant = 0
        amountTextview.text = AppConstants.unitValue
    }
    
    private func initInstance(){
        if(viewModel == nil){
            viewModel = ViewModel(self)
        }
    }
    
    /*
     * Function to set border on button
     */
    private func setBorderToButton(){
        Utility.shared.setBorderToTableView(view: currencyButton, cornerRadius: 10)
    }
    
    
    // MARK: Methods - IBActions
    @IBAction func currencyButtonClicked(_ sender: UIButton) {
        UIView.animate(withDuration: AppConstants.animationDuration) {
            if(!self.isCurrencyTableviewVisible){
                self.currencyTableviewHeight.constant = 44.0 * 5.0
                self.view.bringSubviewToFront(self.currencyTableview)
                self.isCurrencyTableviewVisible = true
            }else{
                self.currencyTableviewHeight.constant = 0
                self.isCurrencyTableviewVisible = false
            }
            self.view.layoutIfNeeded()
        }
    }
    
    
}

