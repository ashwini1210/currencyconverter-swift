//
//  RatesCollectionViewCell.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class RatesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var currencyRate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setBorderTocell()
    }
    
    /*
     * Function to set border to collection view cell
     */
    private func setBorderTocell(){
        Utility.shared.setBorderToTableView(view: self, cornerRadius: 5)
    }
    
    /*
     * Function to set values on cell Labels
     */
    public func setLabelValues(countryName: String, currencyRate: Double){
        self.countryName.text = String(countryName.suffix(3))
        self.currencyRate.text = String(format:"%.02f",currencyRate)
    }
    
    
    
}
