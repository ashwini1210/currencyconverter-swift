//
//  CollectionView.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    // MARK: Internal Methods
    /*
     * Function to register collection view cell
     */
     internal func registerCollectionView(){
        self.ratesCollectionView.register(UINib.init(nibName: AppConstants.cellName, bundle: nil), forCellWithReuseIdentifier: AppConstants.cellName)
    }
    
    /*
     * Function to set border to collection view
     */
    internal func setBorderToCollectionView(){
        Utility.shared.setBorderToTableView(view: ratesCollectionView, cornerRadius: 5)
    }
    
    
    // MARK: UICollectionView DataSource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let viewModel  = self.viewModel{
            let currencyRate = viewModel.getAllCurrencyRate()
            if let rate = currencyRate{
                return rate.quotes.count
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppConstants.cellName, for: indexPath) as! RatesCollectionViewCell
        if let vm  = self.viewModel{
            if(vm.quotesArray.count>0){
                let key = vm.quotesArray[indexPath.row].key
                let value = vm.calculateCurrencyRate(indexPath: indexPath)
                if let val = value{
                    cell.setLabelValues(countryName: key, currencyRate: val)
                }
                
            }
        }
        return cell
    }
    
    
}
