//
//  RatesTableview.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    // MARK: Custom Methods
    internal func setBorderToTableView(){
        Utility.shared.setBorderToTableView(view: currencyTableview, cornerRadius: 3)
    }
    
    //MARK:- UITableview Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let vm  = self.viewModel{
            let currencyList = vm.getAllCurrencyList()
            if let list = currencyList{
                return list.currencies.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: AppConstants.currencyTableviewIdentifier)
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: AppConstants.currencyTableviewIdentifier)
        }
        if let vm  = self.viewModel{
            let currencyList = vm.getAllCurrencyList()
            if let list = currencyList{
                let sortedList = list.currencies.keys.sorted()
                cell?.textLabel?.text =  Array(sortedList)[indexPath.row]
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.selectionStyle = .none
        if let vm  = self.viewModel{
            let currencyList = vm.getAllCurrencyList()
            if let list = currencyList{
                let sortedList = list.currencies.keys.sorted()
                amountTextview.text = AppConstants.unitValue
                vm.baseRate = (Double(amountTextview.text!))!
                currencyButton.setTitle( Array(sortedList)[indexPath.row], for: .normal)
                vm.displayUpdatedRate()
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
        animateCell()
    }
    
    /*
     * Function to open currency list with animation
     */
    internal func animateCell(){
        UIView.animate(withDuration: 0.5) {
            self.currencyTableviewHeight.constant = 0
            self.isCurrencyTableviewVisible = false
            self.view.layoutIfNeeded()
        }
    }
}
