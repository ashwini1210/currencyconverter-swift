//
//  TextView.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UITextFieldDelegate{
    
    //MARK:- UIResponder Method
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let vm  = self.viewModel{
            vm.hideKeyboard()
        }
    }
    
    //MARK:- UITextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let vm  = self.viewModel{
            vm.showKeyboard()
        }
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if let vm  = self.viewModel{
            vm.displayUpdatedRate()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }

}
