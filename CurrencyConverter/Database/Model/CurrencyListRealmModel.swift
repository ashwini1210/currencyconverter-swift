//
//  CurrencyListModel.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/16.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit
import RealmSwift

class CurrencyListRealmModel: Object {
    @objc dynamic var success = false
    let currencies = List<CurrencyRealmModel>()
}
