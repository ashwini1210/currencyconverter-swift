//
//  CurrencyModel.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/16.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit
import RealmSwift

class CurrencyRealmModel: Object {
    @objc dynamic var currencyAbbreviation = ""
    @objc dynamic var countryName = ""
}
