//
//  CurrencyRateModel.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/16.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit
import RealmSwift

class CurrencyRateRealmModel: Object {
    @objc dynamic var success = false
    @objc dynamic var source = ""
    @objc dynamic var timestamp = 0
    let quotes = List<QuoteRealmModel>()
}
