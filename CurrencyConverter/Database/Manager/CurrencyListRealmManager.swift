//
//  CurrencyListManager.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/16.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit
import RealmSwift

class CurrencyListRealmManager: NSObject {
    
    var realm: Realm
    
    override init() {
        self.realm = try! Realm()
    }
    
    /*
     * Save Currency List in DB
     */
    public func saveCurrencyList(currencyList:CurrencyList){
        try! realm.write {
            let currencyListRealmModel = CurrencyListRealmModel()
            currencyListRealmModel.success = currencyList.success
            
            for (key, value) in currencyList.currencies {
                let currencyRealmModel = CurrencyRealmModel()
                currencyRealmModel.currencyAbbreviation = key
                currencyRealmModel.countryName = value
                currencyListRealmModel.currencies.append(currencyRealmModel)
            }
            realm.add(currencyListRealmModel)
        }
    }
    
    /*
     * Get Currency List from DB
     */
    public func getCurrencyList() -> CurrencyList?{
        let results = realm.objects(CurrencyListRealmModel.self)
        var dict: [String: String] = [:]        
        for item in results{
            for list in item.currencies{
                dict[list.currencyAbbreviation] = list.countryName
            }
            let currencyList = CurrencyList(success: item.success, currencies: dict)
            return currencyList
        }
        return nil
    }
}
