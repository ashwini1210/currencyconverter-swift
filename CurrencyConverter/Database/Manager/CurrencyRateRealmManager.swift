//
//  CurrencyRateManager.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/16.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit
import RealmSwift

class CurrencyRateRealmManager: NSObject {
    
    var realm: Realm
    
    override init() {
        self.realm = try! Realm()
    }
    
    /*
     * Save Currency rate in DB
     */
    public func saveCurrencyRate(currencyRate:CurrencyRate){
        try! realm.write {
            let currencyRateRealmModel = CurrencyRateRealmModel()
            currencyRateRealmModel.success = currencyRate.success
            currencyRateRealmModel.timestamp = currencyRate.timestamp
            currencyRateRealmModel.source = currencyRate.source
            
            for (key, value) in currencyRate.quotes {
                let quoteRealmModel = QuoteRealmModel()
                quoteRealmModel.USDToCuntryName = key
                quoteRealmModel.rate = value
                currencyRateRealmModel.quotes.append(quoteRealmModel)
            }
            realm.add(currencyRateRealmModel)
        }
    }
    
    /*
     * Get Currency rate from DB
     */
    public func getCurrencyRate() -> CurrencyRate?{
        let results = realm.objects(CurrencyRateRealmModel.self)
        var dict: [String: Double] = [:]
        for item in results{
            for list in item.quotes{
                dict[list.USDToCuntryName] = list.rate
            }
            let currencyRate = CurrencyRate(success: item.success, timestamp: item.timestamp, source: item.source, quotes: dict)
            return currencyRate
            
        }
        return nil
    }
    
    /*
     * Delete data from DB
     */
    public func deleteData(){
        let result = realm.objects(CurrencyRateRealmModel.self)
        try! realm.write {
            realm.delete(result)
            
        }
    }
}
