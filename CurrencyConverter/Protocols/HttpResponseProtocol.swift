//
//  HttpResponseProtocol.swift
//  CurrencyConverter
//
//  Created by Chinchkhede, Ashwini on 2020/05/15.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import Foundation

protocol HttpResponseProtocol{
    func onHttpResponse(isSuccess:Bool, response:Any?, error: Any?, anyIdentifier:String? )

}
