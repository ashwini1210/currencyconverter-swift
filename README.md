# CurrencyConverter

## Summery

Introducing a Currency Conversion App that empowers users to effortlessly access and explore real-time exchange rates for a diverse 
range of global currencies. Seamlessly designed and intuitively crafted, this app provides an immersive experience that puts the world's financial
markets at your fingertips. 
Stay informed and make informed decisions with the ability to swiftly view exchange rates for any desired currency pair. 
Whether you're a seasoned traveler, a shrewd investor, or simply someone who values a comprehensive understanding of international finance, 
this app is your gateway to accurate and up-to-the-minute currency conversion information.

## Version

This app is developed for iPhone using **Swift 5**

## Build and Runtime Requirements

1. Xcode 11.0 or later
2. iOS 14 or later
3. OS X v10.15 or later

## Configuring the Project

1. Clone the repository
2. command 'pod install' on the Terminal
3. Open CurrencyConverter.xcworkspace

## How it works:

![](https://media.giphy.com/media/LuToywtptwkHHKfcw4/giphy.gif)


## Features:

1. Exchange rates are sourced from the following [API](https://currencylayer.com/documentation) 
2. Users can effortlessly select a preferred currency from an extensive list provided by the API. 
3. In cases where certain currencies are unavailable, conversions are conducted within the app. 
   Minimal floating-point discrepancies are deemed acceptable in such instances.
4. Empowering users, the app allows input of the desired amount for the chosen currency.
5. Upon selection, users are presented with an array of exchange rates corresponding to the chosen currency.
6. The app employs local persistence mechanisms to store exchange rates, with a refresh rate of no more frequent than every 30 minutes, 
   effectively optimizing bandwidth consumption.

## UI Module:
1. An input field allows users to specify their desired monetary amount.
2. A table view elegantly presents the currency options, complemented by a button to highlight the selected currency.
3. A collection view dynamically showcases the computed currency values based on the selected currency.

## Functionality Include:
1. The chosen architectural framework is the Model-View-Controller (MVC) paradigm.
2. Users interact with the app by selecting currencies from a table view.
3. Desired amounts are conveniently set within the dedicated amount text field.
4. Tapping outside triggers the keyboard to retract.
5. Exchange rates seamlessly populate the collection view list.
6. Realm database architecture is implemented for robust local data storage.
7. The server is pinged at intervals of 30 minutes to access the latest currency rates.
8. Rigorous unit test cases are in place to comprehensively evaluate and verify app functionalities.




